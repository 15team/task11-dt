package task11DT;

/**
 * Created by Jingyi on 4/2/15.
 * Final version made by Jingyi on 4/6/15
 */
public class Utility {
    public static double getEntropy(Double[] counts) {
        double sum = 0.0, entropy = 0.0;
        for (double count: counts) {
            sum += count;
        }
        for (double count: counts) {
            if (count > 0)
                entropy -= count / sum * Math.log(count / sum) / Math.log(2);
        }
        return entropy;
    }
    public static double getInfoGain(Double[][] counts) {
        Double[] sumCounts = new Double[counts[0].length];
        Double[] weight = {0.0, 0.0};
        for (int i = 0; i < counts[0].length; i ++) {
            sumCounts[i] = counts[0][i] + counts[1][i];
            weight[0] += counts[0][i];
            weight[1] += counts[1][i];
        }
        double sum = weight[0] + weight[1];
        weight[0] = weight[0] / sum;
        weight[1] = weight[1] / sum;
        return getEntropy(sumCounts) - weight[0] * getEntropy(counts[0]) - weight[1] * getEntropy(counts[1]);
    }

    public static double getIntrinsicVal(Double[][] counts) {
        Double[] sumCounts = {0.0, 0.0};
        for (int i = 0; i < counts[0].length; i ++) {
            sumCounts[0] += counts[0][i];
            sumCounts[1] += counts[1][i];
        }
        return getEntropy(sumCounts);
    }

    public static double getInfoGainRatio(Double[][] counts) {
        return getInfoGain(counts) / getIntrinsicVal(counts);
    }
}

