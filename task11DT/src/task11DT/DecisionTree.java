package task11DT;

/**
 * Created by Jingyi on 4/2/15. 
 * Final version made by Jingyi on 4/6/15
 */
public class DecisionTree {

	// an inner class to store each node of the tree
	class Node {
		int id;
		String type;
		int index;
		String val;
		Node left, right;

		public Node(String type, int index, String val) {
			this.type = type;
			this.index = index;
			this.val = val;
			this.id = counter++;
		}

		public String toString() {
			return "Node" + id + ':' + type + '[' + index + "]:" + val;
		}
	}

	// constructor, input is a dataset and a threshold for minimum number of
	// object pruning
	public DecisionTree(DataSet ds, int threshold) {
		this.threshold = threshold;
		root = getNode(ds);
		buildTree(ds, root);
		printTree(root);
	}

	// build a tree by a give dataset and a node
	private void buildTree(DataSet ds, Node current) {

		// split the given dataset by the given node
		DataSet[] splitDs = ds.split(current.type, current.index, current.val);

		// if either of the subset contains records less than threshold, make
		// the current node as leaf and make the most popular label as predicted
		// label
		if (splitDs[0].getRdCounts() < threshold
				|| splitDs[1].getRdCounts() < threshold) {
			current.type = "Result";
			current.index = -1;
			current.val = ds.popularLabel();
			return;
		}

		// if the subset contains only one kind of label, make its left child as
		// leaf and this label as predicted label
		if (splitDs[0].same()) {
			current.left = new Node("Result", -1, splitDs[0].popularLabel());
		} else {
			// recursion
			current.left = getNode(splitDs[0]);
			buildTree(splitDs[0], current.left);
		}

		// same to the other subset
		if (splitDs[1].same()) {
			current.right = new Node("Result", -1, splitDs[1].popularLabel());
		} else {
			current.right = getNode(splitDs[1]);
			buildTree(splitDs[1], current.right);
		}
	}

	// get the root node for a given dataset
	private Node getNode(DataSet ds) {
		String bestType = "";
		int bestIndex = -1;
		String bestVal = "";
		double bestScore = Double.NEGATIVE_INFINITY;
		for (int i = 0; i < ds.getNumStringFeatures(); i++) {
			for (String val : ds.getValues("String", i)) {
				double score = ds.getInfoGainRatio("String", i, val);
				if (score > bestScore) {
					bestType = "String";
					bestIndex = i;
					bestVal = val;
					bestScore = score;
				}
			}
		}
		for (int i = 0; i < ds.getNumDoubleFeatures(); i++) {
			for (String val : ds.getValues("Double", i)) {
				double score = ds.getInfoGainRatio("Double", i, val);
				if (score > bestScore) {
					bestType = "Double";
					bestIndex = i;
					bestVal = val;
					bestScore = score;
				}
			}
		}
		if (bestType.equals("")) {
			throw new IllegalArgumentException("Exception 4 here：null value");
		}
		return new Node(bestType, bestIndex, bestVal);
	}

	private Node root;
	private int threshold = 0;
	private static int counter = 0;

	public String test(Record record, Node current) {
		switch (current.type) {
		case "String":
			if (record.stringFeatures.get(current.index).equals(current.val)) {
				return test(record, current.left);
			} else {
				return test(record, current.right);
			}
		case "Double":
			double value = Double.parseDouble(current.val);
			if (record.doubleFeatures.get(current.index) < value) {
				return test(record, current.left);
			} else {
				return test(record, current.right);
			}
		case "Result":
			return current.val;
		default:
			throw new IllegalArgumentException("Exception 5 here：null value");
		}
	}

	// after the decision tree is built, use this method to test a given record
	public String test(String line) {
		Record record = new Record(line);
		return test(record, root);
	}

	// using in-order traverse to print all the tree node information
	public void printTree(Node root) {
		if (root != null) {
			printTree(root.left);
			if (root.left != null && root.right != null) {
				System.out.println(root.toString() + "LC: "
						+ root.left.toString() + " RC: "
						+ root.right.toString());
			} else {
				System.out.println("Leaf: " + root.toString());
			}
			printTree(root.right);
		}
	}
}
