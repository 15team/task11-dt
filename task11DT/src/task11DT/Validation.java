package task11DT;

/**
 * Created by Jingyi on 4/2/15. 
 * Final version made by Jingyi on 4/6/15
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

//this class is used for cross validation
public class Validation {
	public static void main(String[] args) throws IOException {

		// import the traning set here
		BufferedReader br = new BufferedReader(new FileReader(
				"trainProdIntro.binary.arff"));
		// set threshold for pruning decision tree
		int threshold = 3;
		// set fold number for doing corss validation
		int foldNo = 10;

		String line;
		ArrayList<String> data = new ArrayList<>();
		ArrayList<String> train = new ArrayList<>();
		ArrayList<String> test;
		ArrayList<ArrayList<String>> subsets = new ArrayList<>();

		// load data
		while ((line = br.readLine()) != null) {
			data.add(line);
		}

		int totalError = 0;
		int total = data.size();
		double accuracy;
		int unitNo = total / foldNo;

		// shuffle dataset and make the order random
		Collections.shuffle(data);

		// divide the training set into a given number of subsets
		for (int i = 1; i < foldNo + 1; i++) {
			ArrayList<String> set = new ArrayList<>();
			for (int m = (i - 1) * unitNo; m < unitNo * i; m++) {
				set.add(data.get(m));
			}
			subsets.add(set);
		}

		for (int i = 0; i < foldNo; i++) {
			int error = 0;

			// get test set each round
			test = subsets.get(i);

			// generate traning set each round
			for (int p = 0; p < foldNo; p++) {
				if (p == i) {
					p++;
				} else {
					train.addAll(subsets.get(p));
				}
			}

			DataSet trainSet = new DataSet();
			for (String f : train) {
				trainSet.feed(f);
			}

			// build decision tree by trainSet
			DecisionTree dt = new DecisionTree(trainSet, threshold);

			// calculate errors
			for (String t : test) {
				System.out.println(t + "\t" + dt.test(t));
				Record rd = new Record(t);
				if (!rd.label.equals(dt.test(t))) {
					error++;
				}
			}
			System.out.println("error number this round:" + error);
			totalError = totalError + error;
			System.out.println("total error number:" + totalError);
		}

		// calculate average accuracy for all the rounds
		accuracy = 1 - (double) totalError / total;
		System.out.println("accuracy:" + accuracy);
	}
}
