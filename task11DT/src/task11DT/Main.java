package task11DT;

/**
 * Created by Jingyi on 4/2/15. 
 * Final version made by Jingyi on 4/6/15
 */

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

//this class is to test a given test set using a given training set
public class Main {

	public static void main(String[] args) throws IOException {
		// import the given training set here
		BufferedReader br = new BufferedReader(new FileReader(
				"trainProdIntro.binary.arff"));
		String line;
		DataSet mayDataSet = new DataSet();
		int threshold = 3;
		while ((line = br.readLine()) != null) {
			mayDataSet.feed(line);
		}
		DecisionTree dt = new DecisionTree(mayDataSet, threshold);

		// import the given test set here
		br = new BufferedReader(new FileReader("testProdIntro.binary.arff"));
		while ((line = br.readLine()) != null) {
			System.out.println(line + "\t" + dt.test(line));
		}
	}
}