package task11DT;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jingyi on 4/2/15. 
 * Final version made by Jingyi on 4/6/15
 */

// this class is used to store every line of the input data
public class Record {
	static List<String> labelValues = new ArrayList<>();
	String line;
	List<String> stringFeatures = new ArrayList<>();
	List<Double> doubleFeatures = new ArrayList<>();
	String label;

	public Record(String line) {
		this.line = line;
		// get separate attributes
		String[] elems = line.split(",");
		int labelIndex = elems.length - 1;

		// classify every attribute element as double or string type, and make
		// two list to store them
		for (int i = 0; i < labelIndex; i++) {
			try {
				doubleFeatures.add(Double.parseDouble(elems[i]));
			} catch (NumberFormatException e) {
				stringFeatures.add(elems[i]);
			}
		}
		// get label
		label = elems[labelIndex];
		if (labelValues.indexOf(label) == -1) {
			labelValues.add(label);
		}
	}

	@Override
	public String toString() {
		return line;
	}
}