package task11DT;

import java.util.*;

/**
 * Created by Jingyi on 4/2/15 
 * Final version made by Jingyi on 4/6/15
 */

// this class is used to store several records as a set and offer some
// calculation and manipulation for those set
public class DataSet {
	public List<Record> records = new ArrayList<>();

	public void feed(Record record) {
		records.add(record);
	}

	public void feed(String line) {
		records.add(new Record(line));
	}

	// calculate information gain ratio for a given node
	public double getInfoGainRatio(String type, int index, String val) {
		Double[][] result = { new Double[Record.labelValues.size()],
				new Double[Record.labelValues.size()] };
		for (int i = 0; i < Record.labelValues.size(); i++) {
			result[0][i] = 0.0;
			result[1][i] = 0.0;
		}

		for (Record record : records) {
			switch (type) {
			case "String":
				if (record.stringFeatures.get(index).equals(val)) {
					int labelIndex = Record.labelValues.indexOf(record.label);
					result[0][labelIndex]++;
				} else {
					int labelIndex = Record.labelValues.indexOf(record.label);
					result[1][labelIndex]++;
				}
				break;
			case "Double":
				double value = Double.parseDouble(val);
				if (record.doubleFeatures.get(index) < value) {
					int labelIndex = Record.labelValues.indexOf(record.label);
					result[0][labelIndex]++;
				} else {
					int labelIndex = Record.labelValues.indexOf(record.label);
					result[1][labelIndex]++;
				}
				break;
			default:
				throw new IllegalArgumentException(
						"Exception 1 here: number format exception");
			}
		}
		return Utility.getInfoGainRatio(result);
	}

	// this method is used to separate dataset by a given node
	public DataSet[] split(String type, int index, String val) {
		DataSet[] result = { new DataSet(), new DataSet() };
		for (Record record : records) {
			switch (type) {
			case "String":
				if (record.stringFeatures.get(index).equals(val)) {
					result[0].feed(record);
				} else {
					result[1].feed(record);
				}
				break;
			case "Double":
				double value = Double.parseDouble(val);
				if (record.doubleFeatures.get(index) < value) {
					result[0].feed(record);
				} else {
					result[1].feed(record);
				}
				break;
			default:
				throw new IllegalArgumentException(
						"Exception 2 here：number format");
			}
		}
		return result;
	}

	public int getNumStringFeatures() {
		return records.get(0).stringFeatures.size();
	}

	public int getNumDoubleFeatures() {
		return records.get(0).doubleFeatures.size();
	}

	public HashSet<String> getValues(String type, int index) {
		HashSet<String> result = new HashSet<>();
		for (Record record : records) {
			switch (type) {
			case "String":
				result.add(record.stringFeatures.get(index));
				break;
			case "Double":
				result.add(record.doubleFeatures.get(index).toString());
				break;
			default:
				throw new IllegalArgumentException(
						"Exception 3 here：null value");
			}
		}
		return result;
	}

	// test if all the records in this dataset has the same label
	public boolean same() {
		for (Record record : records) {
			if (!record.label.equals(records.get(0).label))
				return false;
		}
		return true;
	}

	// chose the label with most number for this dataset
	public String popularLabel() {
		Map<String, Integer> map = new HashMap<>();
		for (Record record : records) {
			Integer count = map.get(record.label);
			map.put(record.label, count != null ? count + 1 : 0);
		}
		return Collections.max(map.entrySet(),
				new Comparator<Map.Entry<String, Integer>>() {
					@Override
					public int compare(Map.Entry<String, Integer> o1,
							Map.Entry<String, Integer> o2) {
						return o1.getValue().compareTo(o2.getValue());
					}
				}).getKey();
	}

	public int getRdCounts() {
		return records.size();
	}
}
